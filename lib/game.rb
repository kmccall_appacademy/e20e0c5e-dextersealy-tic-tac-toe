require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board

  def initialize(player_one, player_two)
    @board = Board.new
    @players = [player_one, player_two]
    @players[0].mark = :X
    @players[1].mark = :O
    @turn = 0
  end

  def play_turn
    player = @players[@turn]
    player.display(@board)
    pos = player.get_move
    @board.place_mark(pos, player.mark)
    switch_players!
  end

  def switch_players!
    @turn = 1 - @turn
  end

  def current_player
    @players[@turn]
  end

  def over?
    @board.over?
  end

end

if __FILE__ == $PROGRAM_NAME
  human = HumanPlayer.new("Dave")
  machine = ComputerPlayer.new("Hal")
  game = Game.new(machine, human)

  until game.over?
    game.play_turn
  end

  puts "Game over!"
  human.display(game.board)

end
