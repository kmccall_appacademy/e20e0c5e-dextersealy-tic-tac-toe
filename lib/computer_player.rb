class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = nil
  end

  def display(board)
    @board = board
  end

  def get_move
    possible_moves = []

    (0...9).each do |idx|
      pos = [idx / 3, idx % 3]
      next unless board.empty?(pos)
      return pos if winning_move?(idx)
      possible_moves << pos
    end

    possible_moves.sample
  end

  def winning_move?(idx)
    return true if almost_win?(board.get_row(idx / 3))
    return true if almost_win?(board.get_column(idx % 3))
    return true if (idx % 4 == 0) && almost_win?(board.get_left_diagonal())
    return true if (idx % 2 == 0) && almost_win?(board.get_right_diagonal())
    false
  end

  def almost_win?(seq)
    return seq.count(mark) == 2
  end
end
