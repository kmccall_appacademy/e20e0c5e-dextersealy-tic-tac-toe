class Board
  attr_reader :grid

  @@winners = [
    [0, 1, 2], [2, 3, 4], [4, 5, 6],
    [0, 3, 6], [1, 4, 7], [2, 5, 8],
    [0, 4, 8], [2, 4, 6]
  ]

  def initialize(grid=nil)
    @grid = grid ? grid : 3.times.map { |i| [nil]*3 }
  end

  def place_mark(pos, mark)
    raise "invalid move" if !empty?(pos)
    self[pos] = mark
  end

  def empty?(pos)
    !self[pos]
  end

  def winner
    flat = grid.flatten
    @@winners.each do |seq|
      return flat[seq.first] if won?(seq.map { |idx| flat[idx] })
    end
    nil
  end

  def over?
    return true if winner || grid.flatten.count(nil) == 0
    false
  end

  def get_row(row)
    grid[row]
  end

  def get_column(column)
    (0..2).to_a.map { |idx| grid[idx][column] }
  end

  def get_left_diagonal()
    (0..2).to_a.map { |idx| grid[idx][idx] }
  end

  def get_right_diagonal()
    (0..2).to_a.map { |idx| grid[idx][grid.length - idx - 1] }
  end

  private

  def won?(seq)
    return true if seq.first && seq.count(seq.first) == seq.length
    false
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    grid[row][col] = mark
  end

end
