class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = nil
  end

  def display(board)
    board.grid.each do |row|
      row.each do |el|
        print (el ? el.to_s : ".").center(3)
      end
      puts
    end
    puts
  end

  def get_move
    print "Where do you want to place your mark: "
    gets.split(",").map { |str| str.strip.to_i }
  end

end
